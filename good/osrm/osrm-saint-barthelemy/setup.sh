#!/usr/bin/env bash
set -e

workdir=$(dirname $0)
namespace=dev-osrm
context=dev00-central-cluster-kikotey-com-admin
chart=hypnoglow/osrm
name=osrm-saint-barthelemy

function dry-run() {
  helm diff upgrade -f values.yaml --namespace ${namespace} ${name} ${chart} --allow-unreleased 
}

function deploy() {
   helm upgrade  --kube-context ${context}  ${name} ${chart} --install --create-namespace --namespace ${namespace} --wait --timeout 25s --values values-saint-barthelemy.yaml --force
}

function confirm() {
  read -r -p "Rollout deployment ? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

helm repo update
function main() {
  #dry-run
  if confirm ; then
    deploy
  else
    echo "rollout abort"
  fi
}

main
