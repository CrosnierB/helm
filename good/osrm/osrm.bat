REM read me : run osrm with the following process
    REM install docker
    REM install osmium
    REM
    REM install image docker : docker pull mediagis/nominatim
    REM run nominatim-backend from local server
    REM run myenvmapping.sh from a directory. it will install all things into local directory then into docker image
    REM
    REM run command line to lauch osm local server later
    REM docker run -t -i -p 5000:5000 -v "${PWD}:/data" osrm/osrm-backend osrm-routed --algorithm mld /data/allmaps.osrm
    REM test : http://127.0.0.1:5000/route/v1/driving/13.388860,52.517037;13.397634,52.529407;13.428555,52.523219?overview=false
    REM in Git bash if needed : docker ps -a | awk '{ print $1,$2 }' | grep stefda | awk '{print $1 }' | xargs -I {} docker rm {}
    REM in Git bash if needed : docker ps -a | awk '{ print $1,$2 }' | grep osrm/osrm | awk '{print $1 }' | xargs -I {} docker rm {}


REM building of the map
REM @echo off
REM echo Path of the repository
REM cd /D "C:\PROJET\kikotey_web_app\Toolkit\osm local server"
REM cd

echo Building of the merged map (+ download osmium if needed)
del *.osrm.*
del *.osm.*
del *.osrm
del *.osm
wget http://download.openstreetmap.fr/extracts/central-america/guadeloupe.osm.pbf
wget http://download.openstreetmap.fr/extracts/central-america/martinique.osm.pbf
wget http://download.openstreetmap.fr/extracts/central-america/saint_martin.osm.pbf
wget http://download.openstreetmap.fr/extracts/central-america/sint_maarten.osm.pbf
wget http://download.openstreetmap.fr/extracts/central-america/saint_barthelemy.osm.pbf
wget http://download.openstreetmap.fr/extracts/south-america/guyane.osm.pbf
wget http://download.openstreetmap.fr/extracts/south-america/guyana.osm.pbf

docker run -it -v "%cd%:/data" stefda/osmium-tool osmium merge /data/guadeloupe.osm.pbf /data/martinique.osm.pbf /data/saint_martin.osm.pbf /data/sint_maarten.osm.pbf /data/saint_barthelemy.osm.pbf /data/guyane.osm.pbf /data/guyana.osm.pbf -o /data/allmaps.osm.pbf
echo Map builded....OK

REM building and launching of the back end
echo Building of the back end (+ download osrm-backend if needed)
docker image ls
docker run -t -v "%cd%:/data" osrm/osrm-backend osrm-extract -p /opt/car.lua /data/allmaps.osm.pbf
docker run -t -v "%cd%:/data" osrm/osrm-backend osrm-partition /data/allmaps.osrm
docker run -t -v "%cd%:/data" osrm/osrm-backend osrm-customize /data/allmaps.osrm
echo Back end is build...OK

echo Launching of the back end
REM docker run -t -i -p 5000:5000 -v "%cd%:/data" osrm/osrm-backend osrm-routed --algorithm mld /data/allmaps.osrm
echo Back end launched on port 5000.... OK
