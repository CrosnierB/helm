replicaCount: 1

nameOverride: kratos

serviceExtra:
  ports: []

image:
  repository: oryd/kratos
  tag: v0.13.0
  pullPolicy: Always
containerCommand:
- kratos
- serve
- --dev
- --config
- $(CONFIG)
- --watch-courier

initContainers:
- name: migrate
  image: oryd/kratos:v0.13.0
  imagePullPolicy: Always
  args:
  - -c
  - $(CONFIG)
  - migrate
  - sql
  - -e
  - --yes

rbac:
  create: false
  enabled: false
  role: ''

kind: deployment # sts

podLabels:
  auth.kikotey.io: kratos

service:
  name: kratos
  internalPort: 4433
  externalPort: 80
  type: ClusterIP

externalsvc: {}

#resources:
#  limits:
#    cpu: 300m
#    memory: 256Mi
#  requests:
#    cpu: 100m
#    memory: 64Mi

lifecycle:
  enabled: false

rules:
  enabled: false

extraContainerPort:
- name: admin
  port: 4434
  target: 4434

ingress:
  annotations:
    #kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/enable-cors: "true"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/use-regex: "true"
    nginx.ingress.kubernetes.io/cors-allow-origin: http://kratos.kikotey.com
    nginx.ingress.kubernetes.io/ingress.class: addon-http-application-routing
    class: nginx
  enabled: true
  extraPaths:
    - backend:
        serviceName: kratos
        servicePort: 80
      path: /sessions
    - backend:
        serviceName: kratos
        servicePort: 80
      path: /verification
    - backend:
        serviceName: kratos
        servicePort: 80
      path: /schemas
    - backend:
        serviceName: kratos
        servicePort: 80
      path: /recovery
    - backend:
        serviceName: kratos
        servicePort: 80
      path: /error
  host: 'kratos.kikotey.com'
  path: /self-service
  prefix: "nginx.ingress.kubernetes.io"

revision:
  enabled: true

#tolerations:
#- effect: NoSchedule
#  key: dedicated
#  operator: Equal
#  value: appli

tolerations:
- effect: NoSchedule
  key: node-role.kubernetes.io/master

livenessProbe:
  failureThreshold: 3
  httpGet:
    path: /health/alive
    port: 4433
    scheme: HTTP
  periodSeconds: 10
  successThreshold: 1
  timeoutSeconds: 1

readinessProbe:
  failureThreshold: 3
  httpGet:
    path: /health/ready
    port: 4433
    scheme: HTTP
  periodSeconds: 10
  successThreshold: 1
  timeoutSeconds: 1

metrics:
  enabled: true
  path: '/metrics/prometheus'
  port: 4433

#nodeSelector:
#  role: agent

secrets:
- key: secrets-default
  kind: env
  name: SECRETS_DEFAULT
  value: f80BQJpVH6US2Uxz19qUSKJkrMOm75I8
- key: secrets-cookie
  kind: env
  name: SECRETS_COOKIE
  value: HztKxmvA9YYkgpkGz7kaSJTltR1FskTW4EiKs01JK4L1Tlcu6tAcz28UVop5mUeH
- key: secrets-cipher
  kind: env
  name: SECRETS_CIPHER
  value: 3Vs5eMDo1s3C7rEmxe1Xf6S8cfCToZS2
- key: dsn
  kind: env
  name: DSN
  value: postgres://kratos:pO1WlJPLAFoxfs24ELyCL4iPNIDKzrID@kratos-postgresql:5432/kratos?sslmode=disable&max_conns=20&max_idle_conns=4

configs:
- key: kratos.yaml
  kind: volume
  name: CONFIG
  path: /kratos/config
  subPath: "disabled"
  value: |
    version: v0.13.0
    identity:
      default_schema_id: default
      schemas:
        - id: default
          url: file:///kratos/schemas/identity/identity.schema.json
        - id: person
          url: file:///kratos/schemas/person/person.schema.json
        - id: dmgp
          url: file:///kratos/schemas/dmgp/dmgp.schema.json
        - id: organization
          url: file:///kratos/schemas/organization/organization.schema.json
        - id: scope
          url: file:///kratos/schemas/scope/scope.schema.json
    serve:
      public:
        base_url: http://kratos.kikotey.com
        request_log:
          disable_for_health: true
        cors:
          enabled: true
          allowed_origins:
            - http://kratos.kikotey.com
      admin:
        request_log:
          disable_for_health: true
        base_url: http://kratos.dev-kratos.svc.cluster.local:4434/
    session:
      cookie:
        domain: kratos.kikotey.com
        persistent: true
    selfservice:
      default_browser_return_url: http://kratos.kikotey.com/
      methods:
        code: # or link
          enabled: true
          config:
            # Defines how long the verification or the recovery code is valid for (default 1h)
            lifespan: 15m
        link:
          config:
            lifespan: 1h
            base_url:  http://kratos.kikotey.com
          enabled: true
        password:
          enabled: true
          config:
            haveibeenpwned_enabled: false
            min_password_length: 6
            identifier_similarity_check_enabled: true
        totp:
          config:
            issuer: Kratos
          enabled: false
        lookup_secret:
          enabled: false
        webauthn:
          config:
            passwordless: true
            rp:
              display_name: kikotey
              id: localhost
              origin: http://kratos.kikotey.com
          enabled: false
        oidc:
          enabled: false
      flows:
        error:
          ui_url: http://kratos.kikotey.com/error
        settings:
          ui_url: http://kratos.kikotey.com/settings
          privileged_session_max_age: 15m
        recovery:
          enabled: true
          lifespan: 15m
          ui_url: http://kratos.kikotey.com/recovery
          use: code
          notify_unknown_recipients: true
          after:
            hooks:
            - hook: revoke_active_sessions
        verification:
          enabled: true
          ui_url: http://kratos.kikotey.com/verification
          use: code
          after:
            default_browser_return_url: http://kratos.kikotey.com/
          notify_unknown_recipients: false
        logout:
          after:
            default_browser_return_url: http://kratos.kikotey.com/auth/login
        login:
          ui_url: http://kratos.kikotey.com/auth/login
          lifespan: 10m
          after:
            hooks:
            - hook: require_verified_address
        registration:
          lifespan: 10m
          ui_url: http://kratos.kikotey.com/auth/registration
          after:
            password:
              hooks:
                - hook: session
                - hook: show_verification_ui
            oidc:
              hooks:
                - hook: session
    log:
      level: debug
      format: text
      leak_sensitive_values: true
    ciphers:
      algorithm: xchacha20-poly1305
    hashers:
      argon2:
        parallelism: 1
        memory: 128MB
        iterations: 2
        salt_length: 16
        key_length: 16
    courier:
      smtp:
        connection_uri: smtp://AKIARMPQTVP5VZFECRGM:BFxgJNu9h3imW8Sbq2biSJSH2Ma8+ZMIEaLJLe3xYRvO@email-smtp.us-east-1.amazonaws.com:587/?skip_ssl_verify=false
        from_address: contact@kikotey.com
        from_name: "Kikotey Inc"
        local_name: kikotey.com 
      message_retries: 5
      sms:
        from: "+13614018028"
        request_config:
          url: http://api.twilio.com/2010-04-01/Accounts/AC58827afa64d6870d4cb9c6d865c2aee8/Messages.json
          method: POST
          body: file:///kratos/template/sms/twilio.request.jsonnet
          auth:
            type: api_key
            config:
              name: AC58827afa64d6870d4cb9c6d865c2aee8
              value: 164f8eb6f14555ef7ad20a0c0ae10dc3
              in: header
        enabled: true
      templates:
        verification:
          valid:
            email:
              subject: file:///kratos/template/mail/verification/valid/subject/verification-valid-subject.gotmpl
              body:
                html: file:///kratos/template/mail/verification/valid/body/html/verification-valid-body.html.gotmpl
                plaintext: file:///kratos/template/mail/verification/valid/body/plaintext/verification-valid-body.plaintext.gotmpl
          invalid:
            email:
              subject: file:///kratos/template/mail/verification/invalid/subject/verification-invalid-subject.gotmpl
              body:
                html:  file:///kratos/template/mail/verification/invalid/body/plaintext/verification-invalid-body.plaintext.gotmpl
                plaintext: file:///kratos/template/mail/verification/invalid/body/plaintext/verification-invalid-body.plaintext.gotmpl
        recovery:
          valid:
            email:
              subject: file:///kratos/template/mail/recovery/valid/subject/recovery-valid-subject.gotmpl
              body:
                html: file:///kratos/template/mail/recovery/valid/body/html/recovery-valid-body.html.gotmpl
                plaintext: file:///kratos/template/mail/recovery/valid/body/plaintext/recovery-valid-body.plaintext.gotmpl
          invalid:
            email:
              subject: file:///kratos/template/mail/recovery/invalid/subject/recovery-invalid-subject.gotmpl
              body:
                html: file:///kratos/template/mail/recovery/invalid/body/plaintext/recovery-invalid-body.plaintext.gotmpl
                plaintext: file:///kratos/template/mail/recovery/invalid/body/plaintext/recovery-invalid-body.plaintext.gotmpl
- key: verification-valid-subject.gotmpl
  name: TEMPLATE-MAILVERIFICATIONVALIDSUB
  path: /kratos/template/mail/verification/valid/subject
  subPath: "disabled"
  value: |-
      Please verify your email address
- key: verification-valid-body.html.gotmpl
  name: TEMPLATE-MAILVERIFICATIONVALIDBODHTML
  path: /kratos/template/mail/verification/valid/body/html
  subPath: "disabled"
  value: |-
      Hi, please verify your account by clicking the following link:
      <a href="{{ .VerificationURL }}">{{ .VerificationURL }}</a>
- key: verification-valid-body.plaintext.gotmpl
  name: TEMPLATE-MAILVERIFICATIONVALIDBODPLAINTEXT
  path: /kratos/template/mail/verification/valid/body/plaintext
  subPath: "disabled"
  value: |-
      Hi, please verify your account by clicking the following link:
      {{ .VerificationURL }}
- key: verification-invalid-subject.gotmpl
  name: TEMPLATE-MAILVERIFICATIONINVALIDSUB
  path: /kratos/template/mail/verification/invalid/subject
  subPath: "disabled"
  value: |-
      Someone tried to verify this email address
- key: verification-invalid-body.plaintext.gotmpl
  name: TEMPLATE-MAILVERIFICATIONINVALIDBODPLAINTEXT
  path: /kratos/template/mail/verification/invalid/body/plaintext
  subPath: "disabled"
  value: |-
      Hi,
      someone asked to verify this email address, but we were unable to find an account for this address.
      If this was you, check if you signed up using a different address.
      If this was not you, please ignore this email.
- key: recoverycode-valid-subject.gotmpl
  name: TEMPLATE-MAILRECOVERYCODEVALIDSUB
  path: /kratos/template/mail/recoverycode/valid/subject
  subPath: "disabled"
  value: |-
      Recover in your account [OTP]
- key: recoverycode-valid-body.plaintext.gotmpl
  name: TEMPLATE-MAILRECOVERYCODEVALIDBODPLAINTEXT
  path: /kratos/template/mail/recoverycode/valid/body/plaintext
  subPath: "disabled"
  value: |-
      Hi,
      please recover access to your account by entering the following code:
      {{ .RecoveryCode }}
- key: recovery-valid-subject.gotmpl
  name: TEMPLATE-MAILRECOVERYVALIDSUB
  path: /kratos/template/mail/recovery/valid/subject
  subPath: "disabled"
  value: |-
      Recover access to your account
- key: recovery-valid-body.html.gotmpl
  name: TEMPLATE-MAILRECOVERYVALIDBODHTML
  path: /kratos/template/mail/recovery/valid/body/html
  subPath: "disabled"
  value: |-
      Hi,
      please recover access to your account by clicking the following link:
      <a href="{{ .RecoveryURL }}">{{ .RecoveryURL }}</a>
- key: recovery-valid-body.plaintext.gotmpl
  name: TEMPLATE-MAILRECOVERYVALIDBODPLAINTEXT
  path: /kratos/template/mail/recovery/valid/body/plaintext
  subPath: "disabled"
  value: |-
      Hi,
      please recover access to your account by clicking the following link:
      {{ .RecoveryURL }}
- key: recovery-invalid-subject.gotmpl
  name: TEMPLATE-MAILRECOVERYINVALIDSUB
  path: /kratos/template/mail/recovery/invalid/subject
  subPath: "disabled"
  value: |-
      Account access attempted
- key: recovery-invalid-body.plaintext.gotmpl
  name: TEMPLATE-MAILRECOVERYINVALIDBODPLAINTEXT
  path: /kratos/template/mail/recovery/invalid/body/plaintext
  subPath: "disabled"
  value: |-
      Hi,
      you (or someone else) entered this email address when trying to recover access to an account.
      However, this email address is not on our database of registered users and therefore the attempt has failed.
      If this was you, check if you signed up using a different address.
      If this was not you, please ignore this email.
- key: twilio.request.jsonnet
  name: TEMPLATE-SMS
  path: /kratos/template/sms
  subPath: "disabled"
  value: |-
      function(ctx) {
        from: ctx.From,
        to: ctx.To,
        body: ctx.Body
      }
- key: identity.schema.json
  name: SCHEMAS-IDE
  path: /kratos/schemas/identity
  subPath: "disabled"
  value: |-
    {
      "$id": "http://schema.kikotey.com/identity.schema.json",
      "$schema": "http://json-schema.org/draft-07/schema#",
      "definitions": {
        "role": {
          "type": "array",
          "item": {
            "type": "object",
            "properties": {
              "key": {
                "type": "integer"
              },
              "value": {
                "type": "string"
              }
            }
          }
        }
      },
      "title": "Identity",
      "type": "object",
      "properties": {
        "traits": {
          "type": "object",
          "properties": {
            "email": {
              "type": "string",
              "format": "email",
              "title": "E-Mail",
              "minLength": 3,
              "ory.sh/kratos": {
                "credentials": {
                  "password": {
                    "identifier": true
                  }
                },
                "verification": {
                  "via": "email"
                },
                "recovery": {
                  "via": "email"
                }
              }
            },
            "phone": {
              "title": "Phone",
              "type": "string",
              "format": "tel",
              "ory.sh/kratos": {
                "credentials": {
                  "password": {
                    "identifier": true
                  }
                }
              }
            },
            "name": {
              "type": "object",
              "properties": {
                "first": {
                  "title": "First Name",
                  "type": "string"
                },
                "last": {
                  "title": "Last Name",
                  "type": "string"
                }
              }
            },
            "is_deleted": {
              "type": "string",
              "title": "User is deleted ?"
            },
            "dmgp": {
              "type": "string",
              "title": "The dmgp schema id"
            },
            "person": {
              "type": "string",
              "title": "The person schema id"
            },
            "privates_organizations": {
              "type": "array",
              "title": "The privates_organizations list schema id",
              "minItems": 0,
              "items": {
                "type": "string"
              }
            },
            "lang": {
              "type": "string",
              "title": "Your preferred language"
            },
            "accepted_tos": {
              "type": "string",
              "title": "Accept Terms of Service"
            },
            "roles": {
              "type": "object",
              "properties": {
                "organizations": {
                  "title": "List of your organization rules",
                  "$ref": "#/definitions/role"
                },
                "scopes": {
                  "title": "List of your scope rules",
                  "$ref": "#/definitions/role"
                },
                "private_account": {
                  "title": "List of your individual account rules depending you are the owner of an account",
                  "$ref": "#/definitions/role"
                },
                "affiliate_account": {
                  "title": "List of the account rules to which you are affiliated according to your membership in an organization or a field of application ",
                  "$ref": "#/definitions/role"
                }
              }
            }
          },
          "required": [
            "accepted_tos"
          ],
          "additionalProperties": {}
        }
      }
    }
- key: person.schema.json
  name: SCHEMAS-PER
  path: /kratos/schemas/person
  subPath: "disabled"
  value: |-
    {
      "$id": "http://schema.kikotey.com/person.schema.json",
      "$schema": "http://json-schema.org/draft-07/schema#",
      "title": "Person",
      "type": "object",
      "properties": {
        "traits": {
          "type": "object",
          "properties": {
            "identity_associate": {
              "title": "Associate default schema id ",
              "type": "string"
            },
            "favorites": {
              "type": "object",
              "properties": {
                "description": {
                  "title": "Adress of this favorite",
                  "type": "string"
                },
                "iteration": {
                  "title": "Position of this favorite in the list",
                  "type": "integer"
                },
                "lat": {
                  "title": "Latitude geographic coordinates",
                  "type": "number"
                },
                "lng": {
                  "title": "Longitude geographic coordinates",
                  "type": "number"
                },
                "name": {
                  "title": "Name of this favorite",
                  "type": "string"
                }
              }
            },
            "approved": {
              "type": "boolean",
              "title": "This user is approved ?"
            },
            "bookingCurrentServe": {
              "type": "string",
              "format": "uuid",
              "title": "Current booking id of this user."
            },
            "co2": {
              "type": "integer",
              "title": "CO2 savings"
            },
            "discount": {
              "type": "integer",
              "title": "Reduction accorde"
            },
            "driverAvailable": {
              "type": "boolean",
              "title": "This user is available to drive ?"
            },
            "flexAvailable": {
              "type": "boolean",
              "title": "This user is available to drive in flexmode ?"
            },
            "setFlexAvailable": {
              "type": "boolean",
              "title": "This user is available to drive in flexmode ?"
            },
            "licenceApproved": {
              "type": "boolean",
              "title": "Is this user's driver's license valid ?"
            },
            "licenceOtherInfo": {
              "type": "string",
              "title": "Other information about this user's driver's license"
            },
            "mobile": {
              "type": "string",
              "title": "Phone number of this user"
            },
            "nbMessages": {
              "type": "integer",
              "title": "Total number of messages exchanged"
            },
            "nbRiders": {
              "type": "integer",
              "title": "Total number of Riders in the car"
            },
            "needHelpDriver": {
              "type": "boolean",
              "title": "This user is asking for help as a driver ?"
            },
            "needHelpRider": {
              "type": "boolean",
              "title": "This user is asking for help as a rider ?"
            },
            "referralId": {
              "type": "string",
              "title": "referal id of the user"
            },
            "scoreDriver": {
              "type": "integer",
              "title": "Total points for driver loyalty"
            },
            "scoreRider": {
              "type": "integer",
              "title": "Total points for rider loyalty"
            },
            "userModeDriver": {
              "type": "boolean",
              "title": "This user is in driver mode ?"
            },
            "userPlatform": {
              "type": "string",
              "title": "User phone OS is ANDROID or APPLE ?"
            },
            "userRef": {
              "type": "string",
              "format": "uuid",
              "title": "Id of this user",
              "minLength": 3
            },
            "usertype": {
              "type": "string",
              "title": "Type user usage"
            },
            "profileImage": {
              "type": "string",
              "title": "Link to profile images"
            },
            "licenceImageRecto": {
              "type": "string",
              "title": "Link to licence recto images"
            },
            "licenceImageVerso": {
              "type": "string",
              "title": "Link to licence verso images"
            },
            "licenceControleSelfiePictureFace": {
              "type": "string",
              "title": "Link to face Selfie images for controle licence"
            },
            "licenceControleProfilePicture": {
              "type": "string",
              "title": "Link to profile Selfie images for controle licence"
            }
          },
          "additionalProperties": {}
        }
      }
    }
- key: dmgp.schema.json
  name: SCHEMAS-DMG
  path: /kratos/schemas/dmgp
  subPath: "disabled"
  value: |-
    {
      "$id": "http://schema.kikotey.io/dmgp.schema.json",
      "$schema": "http://json-schema.org/draft-07/schema#",
      "title": "Dmgp",
      "type": "object",
      "properties": {
        "traits": {
          "type": "object",
          "properties": {
            "identity_associate": {
              "title": "Associate default schema id ",
              "type": "string"
            },
            "id": {
              "required": [
                "id"
              ],
              "type": "string",
              "title": "The user Id",
              "minLength": 3,
              "ory.sh/kratos": {
                "credentials": {
                  "password": {
                    "identifier": true
                  }
                }
              }
            },
            "address": {
              "type": "object",
              "required": [
                "street_number",
                "street",
                "postcode",
                "locality",
                "state",
                "country"
              ],
              "properties": {
                "street_number": {
                  "title": "Your street number",
                  "type": "integer"
                },
                "street": {
                  "title": "Your street name",
                  "type": "string"
                },
                "postcode": {
                  "title": "Your postal code number",
                  "type": "integer"
                },
                "others_address_informations": {
                  "title": "Others address informations, apartment, stage, unit, number",
                  "type": "string"
                },
                "locality": {
                  "title": "Your city",
                  "type": "string"
                },
                "state": {
                  "title": "Your state, nation",
                  "type": "string"
                },
                "country": {
                  "title": "Your Country",
                  "type": "string"
                }
              }
            },
            "gender": {
              "title": "Your gender, m, f, mtof, ftom",
              "type": "string"
            },
            "birth": {
              "type": "object",
              "required": [
                "year",
                "day",
                "month"
              ],
              "properties": {
                "year": {
                  "title": "Year of your birth",
                  "type": "integer"
                },
                "month": {
                  "title": "Month of your birth",
                  "type": "integer"
                },
                "day": {
                  "title": "Day of your birth",
                  "type": "integer"
                },
                "country": {
                  "title": "Country you born",
                  "type": "string"
                }
              }
            },
            "csp": {
              "title": "Your professional status, employer, cadre",
              "type": "string"
            },
            "function": {
              "title": "Name of your professional function",
              "type": "string"
            },
            "family_structure": {
              "title": "You are mother, dad",
              "type": "string"
            },
            "additionalProperties": {}
          }
        }
      }
    }
- key: scope.schema.json
  name: SCHEMAS-SCO
  path: /kratos/schemas/scope
  subPath: "disabled"
  value: |-
    {
      "$id": "http://schema.kikotey.com/scope.schema.json",
      "$schema": "http://json-schema.org/draft-07/schema#",
      "title": "Scope",
      "type": "object",
      "properties": {
        "traits": {
          "type": "object",
          "properties": {
            "id": {
              "type": "integer",
              "title": "Id of your scope",
              "minLength": 3,
              "ory.sh/kratos": {
                "credentials": {
                  "password": {
                    "identifier": true
                  }
                }
              }
            },
            "organization_associate": {
              "type": "integer",
              "title": "Id of the organization to scope",
              "minLength": 3
            },
            "name": {
              "type": "string",
              "title": "Name of your scope",
              "minLength": 3
            },
            "accounts": {
              "type": "array",
              "title": "Your accounts list",
              "minItems": 1,
              "items": {
                "type": "string"
              }
            }
          },
          "required": [
            "id",
            "organization_associate",
            "name",
            "accounts"
          ],
          "additionalProperties": {}
        }
      }
    }
- key: organization.schema.json
  name: SCHEMAS-ORG
  path: /kratos/schemas/organization
  subPath: "disabled"
  value: |-
    {
      "$id": "http://schema.kikotey.com/organization.schema.json",
      "$schema": "http://json-schema.org/draft-07/schema#",
      "title": "Organization",
      "type": "object",
      "properties": {
        "traits": {
          "type": "object",
          "properties": {
            "id": {
              "type": "integer",
              "title": "Id of your organization",
              "minLength": 3,
              "ory.sh/kratos": {
                "credentials": {
                  "password": {
                    "identifier": true
                  }
                }
              }
            },
            "identity_associate": {
              "title": "Associate default schema id ",
              "type": "string"
            },
            "name": {
              "type": "string",
              "title": "Name of your organization",
              "minLength": 3
            },
            "accounts": {
              "title": "Your accounts list",
              "type": "array",
              "minItems": 1,
              "items": {
                "type": "string"
              }
            }
          },
          "required": [
            "id",
            "name",
            "accounts",
            "identity_associate"
          ],
          "additionalProperties": {}
        }
      }
    }

postgresql:
  enabled: true
  global:
    postgresql:
      postgresqlDatabase: kratos
      postgresqlUsername: kratos
      postgresqlPassword: pO1WlJPLAFoxfs24ELyCL4iPNIDKzrID
  postgresqlPostgresPassword: kMo72sh7XTSczUjdaJapJqMPdAxcLv8T
  metrics:
    enabled: true
  persistence:
    size: 1Gi

mongodb:
  enabled: false
  rbac:
    create: false
  podSecurityPolicy:
    create: false
mongo-express:
  enabled: false
  podSecurityPolicy:
    create: false
  mongodb:
    rbac:
      create: false
    podSecurityPolicy:
      create: false
redis:
  enabled: false
redisinsight:
  enabled: false
minio:
  enabled: false
solr:
  enabled: false
#  global:
#    authentication:
#      adminPasswor: 123456abcdd
