#!/bin/sh

# Exit immediately if a command exits with a non-zero status:
set -e

IMAGE=nexusrepository-kikopool-devops-nexus-repository-manager.devops-tools.svc.cluster.local:9001/repository/docker-registry/docusaurus-dev
TOKEN=_Fjk1608
IDENTIFIANT=admin
oldnum=`cut -d ',' -f2 .increment`
newnum=`expr $oldnum + 1`
TAG=1.0.$newnum
gitea_url=localhost:3000
username_gitea=gitea_admin
password_gitea=r8sA8CPHD9!bt6d
#registry_url=nexusrepository-kikopool-devops-nexus-repository-manager.devops-tools.svc.cluster.local:9002
registry_url=localhost:9001

echo ">>>>>>>>> CLONE DOCUSAURUS FROM KIKOTEY GITHUB ACCOUNT"
git clone https://github.com/kikotey/docusaurus.git

echo ">>>>>>>>> CLONE AND BUILD DOCS, BLOG MARKDOWNS AND CONFIGURATIONS"
git clone http://$username_gitea:$password_gitea@$gitea_url/documentation-dev/docs && rm -Rf docs/.git
git clone http://$username_gitea:$password_gitea@$gitea_url/documentation-dev/blog  && rm -Rf blog/.git
git clone http://$username_gitea:$password_gitea@$gitea_url/documentation-dev/configurations && rm -Rf configurations/.git
cp -i -a docs docusaurus/markdowns/docs
cp -i -a blog docusaurus/markdowns/blog
cp -i -a configurations docusaurus/configurations/

echo ">>>>>>>>> The tag is $TAG"
  docker -H tcp://127.0.0.1:2375 build -t $IMAGE:$TAG -t $IMAGE:latest ./docusaurus/. \
  && sed -i "s/$oldnum\$/$newnum/g" .increment

echo ">>>>>>>>> Login to registry with username $IDENTIFIANT"
  docker -H tcp://127.0.0.1:2375 login $registry_url/repository/docker-registry -u $IDENTIFIANT -p $TOKEN

echo ">>>>>>>>> Push image to the registry $IMAGE"
  docker push -H tcp://127.0.0.1:2375 $IMAGE:latest
  docker push -H tcp://127.0.0.1:2375 $IMAGE:$TAG

echo ">>>>>>>>> The new tag is $TAG"
echo ">>>>>>>>> Deploy image $IMAGE with latest tag in the K8S cluster"
echo Hello ! Do you want deploy? Tape: yes or no
read varRep

if [[ "$varRep" == "yes" ]]; then
  ./setup.sh
fi
