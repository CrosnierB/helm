#!/usr/bin/env bash
set -e

workdir=$(dirname $0)
namespace=dev-bkd-booking
context=dev00-central-cluster-kikotey-com
chart=bitnami/postgresql
name=pg-kikopool-bos

function dry-run() {
  helm diff upgrade --kube-context ${context}  --namespace ${namespace} --allow-unreleased -f values.yaml ${name} ${chart}
}

function deploy() {
  helm upgrade --kube-context ${context}  --install --create-namespace --namespace ${namespace} --render-subchart-notes --reset-values -f values.yaml ${name} ${chart}
}

function check_dependencies() {
  if [ -z "$(helm plugin list | grep '^diff')" ]
  then
    helm plugin install https://github.com/databus23/helm-diff
  fi
}

function check_repo() {
  if [ -z "$( helm repo list | grep bitnami )" ]
  then
    helm repo add bitnami https://charts.bitnami.com/bitnami
  fi
  helm repo update
}

function confirm() {
  read -r -p "Rollout deployment ? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

function main() {
  #check_dependencies
  check_repo
  #dry-run
  if confirm ; then
    deploy
  else
    echo "rollout abort"
  fi
}

main
