#!/bin/sh

# Exit immediately if a command exits with a non-zero status:
set -e

IMAGE=kikotey/ubuntu-workspace
TOKEN=dckr_pat_6zu27-N3kWJ7jtZyCWgv2H59GUE
IDENTIFIANT=kikotey
TAG=$( docker images | awk '($1 == "kikotey/ubuntu-workspace") {$2 += .01; print $2;  exit}')

 find ./docker-ubuntu-novnc/. -type f -print0 | xargs -0 dos2unix

if [[ "$(docker images -q $IMAGE 2> /dev/null)" == "" ]]; then
  echo ">>>>>>>>> Images $IMAGE not exist, this is the first build"
  TAG=1.0.0
  docker build -t $IMAGE:$TAG -t $IMAGE:latest ./docker-ubuntu-novnc/. 
else
  echo ">>>>>>>>> Images $IMAGE exist, this is a incremental tag build"
  echo ">>>>>>>>> The new tag is $TAG"
  docker build -t $IMAGE:$TAG -t $IMAGE:latest ./docker-ubuntu-novnc/.
fi

echo ">>>>>>>>> Login to registry with username $IDENTIFIANT"
docker login -u $IDENTIFIANT -p $TOKEN

echo ">>>>>>>>> Push image to the registry $IMAGE"
docker push $IMAGE:latest
docker push $IMAGE:$TAG


echo ">>>>>>>>> The new tag is $TAG"
echo ">>>>>>>>> Deploy image $IMAGE with latest tag in the K8S cluster"
echo Hello ! Do you want deploy? Tape: yes or no
read varRep

if [[ "$varRep" == "yes" ]]; then
  ./setup.sh
fi
