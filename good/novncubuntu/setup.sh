#!/usr/bin/env bash
set -e

workdir=$(dirname $0)

#namespace=workspace-guest-1608

#######
namespace=workspace-jack-jcdb-1608
#firstname=Jack
#name="CROSNIER DE BELLAISTRE"
#initial=CDBJ
#email=jack.crosnierdebellaistre@kikotey.com

#######
#namespace=workspace-christian-cfo-1608
#firstname=Christian
#name="FIGARO"
#initial=CFO
#email=christian.figaro@kikotey.com

#######
context=dev00-central-cluster-kikotey-com
chart=app-0.9.7/app
name=desktop-local-ubuntu

function dry-run() {
  helm diff upgrade -f values.yaml --namespace ${namespace} ${name} ${chart} --allow-unreleased
}

function deploy() {
  #helm upgrade --kube-context ${context}  --install --create-namespace --namespace ${namespace} --set USERNAME=${firstname}-${initial} --set USERID=${namespace} --set USER_INITIAL=${initial} --set GIT_AUTHOR_NAME="${firstname} ${name}" --set GIT_COMMITTER_NAME="${firstname} ${name}" --set GIT_AUTHOR_EMAIL=${email} --set GIT_COMMITTER_EMAIL=${email} --set USER_NAMESPACE=${namespace}  --render-subchart-notes -f values.yaml ${name} . --force
  helm upgrade --kube-context ${context}  --install --create-namespace --namespace ${namespace}  --render-subchart-notes -f values.yaml ${name} . --force
}

function confirm() {
  read -r -p "Rollout deployment ? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

helm repo update
function main() {
#  dry-run
  if confirm ; then
    deploy
  else
    echo "rollout abort"
  fi
}

main
