{{- define "app.apiversion" -}}
{{- $dictionary := dict "job" "batch/v1" "deployment" "apps/v1" "deploy" "apps/v1" "statefulSet" "apps/v1" "sts" "apps/v1" -}}
{{- default "apps/v1" (get $dictionary (lower .Values.kind)) -}}
{{- end -}}