{{- $image:= (include "app.image" . ) }}
{{- $secretVolumeMount := (include "app.secretVolumeMounts" .) }}
{{- $configmapVolumeMount := (include "app.configmapVolumeMounts" .) }}
{{- $volumeMounts := .Values.volumeMounts }}
{{- $persistence := .Values.persistence }}
{{- $env := .Values.env }}
{{- $secrets := .Values.secrets }}
{{- $configs := .Values.configs }}
{{- $fullname := (include "app.fullname" .) -}}
apiVersion: {{ include "app.apiversion" . }}
kind: {{ include "app.kind" . }}  
metadata:
  {{- if .Values.annotations }}
  annotations:
{{ toYaml .Values.annotations | indent 4 }}
  {{- end }}
  labels:
    {{- include "app.labels" . | nindent 4 }}
    powered-by: "kikotey.com"
  name: {{ template "app.fullname" . }}
spec:
  {{- if eq (include "app.kind" .) "Job" }}
  {{- if .Values.ttlSecondsAfterFinished }}
  ttlSecondsAfterFinished: {{ .Values.ttlSecondsAfterFinished }}
  {{- end }}
  {{- if .Values.backoffLimit }}
  backoffLimit: {{ .Values.backoffLimit }}
  {{- end }}
  {{- if .Values.parallelism }}
  parallelism: {{ .Values.parallelism }}
  {{- end }}
  {{- if .Values.completions }}
  completions: {{ .Values.completions }}
  {{- end }}
  {{- end }}
  {{- if and (ne (include "app.kind" .) "Job") (ne (include "app.kind" .) "DaemonSet") }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  {{- if ne (include "app.kind" .) "Job" }}
  selector:
    matchLabels:
      {{- include "app.selectorLabels" . | nindent 6 }}
  {{- end }}
  {{- if eq (include "app.kind" .) "StatefulSet" }}
  serviceName: {{ template "app.servicename" . }}
  {{- end }}
  template:
    metadata:
      labels:
        {{- include "app.selectorLabels" . | nindent 8 }}
        {{- if .Values.podLabels }}
        {{- toYaml .Values.podLabels | nindent 8 }}
        {{- end }}
      annotations:
        {{- if .Values.metrics.enabled }}
        prometheus.io/path: {{ .Values.metrics.path }}
        prometheus.io/port: {{ .Values.metrics.port | quote }}
        prometheus.io/scrape: "{{ .Values.metrics.enabled }}"
        {{- end }}
        {{- if .Values.annotations }}
{{ toYaml .Values.annotations | indent 8 }}
        {{- end }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
      {{- if .Values.revision.enabled }}
        revision: {{ .Release.Revision | quote }}
      {{- end }}
    spec:
      {{- if .Values.affinity }}
      affinity:
{{ toYaml .Values.affinity | indent 8 }}
      {{- end }}
{{ include "app.pullsecret" . | indent 6}}
      {{- if and .Values.rbac.enabled .Values.rbac.role }}
      serviceAccount: {{ template "app.fullname" . }}
      {{- end }}
      {{- if .Values.initContainers }}
      initContainers:
      {{- range .Values.initContainers }}
      - image: {{ default $image .image }}
        imagePullPolicy: Always
        {{- if .command }}
        command:
{{ toYaml .command | indent 10 }}
        {{- end }}
        {{- if .args }}
        args:
{{ toYaml .args | indent 10 }}
        {{- end }}
        name: {{ .name }}
        {{- if or $.Values.volumeMounts $secretVolumeMount $configmapVolumeMount }}
        volumeMounts:
        {{- if $configmapVolumeMount -}}
{{ $configmapVolumeMount | indent 10 }}
        {{- end }}
        {{- if $secretVolumeMount -}}
{{ $secretVolumeMount | indent 10 }}
        {{- end }}
        {{- if $.Values.volumeMounts }}
{{ toYaml $.Values.volumeMounts | indent 10 }}
        {{- end }}
        {{- end }}
        {{- if or (or $env $secrets) $configs }}
        env:
        {{- if $env }}
{{ toYaml $env | indent 10 }}
        {{- end }}
        {{- if $secrets }}
          {{- range $secrets }}
          {{- if ne (default "env" .kind) "volume" }}
          - name: {{ .name }}
            valueFrom:
              secretKeyRef:
                key: {{ .key }}
                name: {{ $fullname }}
          {{- else }}
          - name: {{ .name }}
            value: {{ .path }}/{{ .key }}
          {{- end }}
          {{- end }}
        {{- end }}
        {{- if $configs }}
          {{- range $configs }}
          - name: {{ .name }}
            value: {{ .path }}/{{ .key }}
          {{- end }}
        {{- end }}
        {{- end }}
      {{- end }}
      {{- end }}
      containers:
      - image: {{ template "app.image" . }}
        imagePullPolicy: {{ default "" .Values.imagePullPolicy | quote }}
        {{- if .Values.containerCommand }}
        command:
{{ toYaml .Values.containerCommand | indent 10 }}
        {{- end }}
        {{- if .Values.containerArgs }}
        args:
{{ toYaml .Values.containerArgs | indent 10 }}
        {{- end }}
        name: {{ include "app.name" . }}
        {{- if or $persistence $volumeMounts $secretVolumeMount $configmapVolumeMount }}
        volumeMounts:
        {{- if $configmapVolumeMount -}}
{{ $configmapVolumeMount | indent 10 }}
        {{- end }}
        {{- if $secretVolumeMount -}}
{{ $secretVolumeMount | indent 10 }}
        {{- end }}
        {{- if $volumeMounts }}
{{ toYaml $volumeMounts | indent 10 }}
        {{- end }}
        {{- if $persistence }}
        {{- range $persistence }}
          - name: {{ .name }}
            mountPath: {{ .path }}
        {{- end }}
        {{- end }}
        {{- end }}
        {{- if or $env $secrets $configs }}
        env:
        {{- if .Values.env }}
{{ toYaml .Values.env | indent 10 }}
        {{- end }}
        {{- if .Values.secrets }}
          {{- range .Values.secrets }}
          {{- if ne (default "env" .kind) "volume" }}
          - name: {{ .name }}
            valueFrom:
              secretKeyRef:
                key: {{ .key }}
                name: {{ $fullname }}
          {{- else }}
          - name: {{ .name }}
            value: {{ .path }}/{{ .key }}
          {{- end }}
          {{- end }}
        {{- end }}
        {{- if $configs }}
          {{- range $configs }}
          - name: {{ .name }}
            value: {{ .path }}/{{ .key }}
          {{- end }}
        {{- end }}
        {{- end }}
        ports:
        - containerPort: {{ .Values.service.internalPort }}
          name: {{ .Values.service.name | trunc 14 }}
          protocol: TCP
          {{- if .Values.service.hostPort }}
          hostPort: .Values.service.hostPort
          {{- end }}
        {{- if and .Values.metrics.enabled ( ne (.Values.metrics.port | int) (.Values.service.internalPort | int ) ) }}
        - containerPort: {{ .Values.metrics.port }}
          name: metrics
          protocol: TCP
        {{- end }}
        {{- if .Values.extraContainerPort }}
        {{- range .Values.extraContainerPort }}
        - containerPort: {{ .port }}
          name: {{ .name }}
          protocol: {{ default "TCP" .protocol }}
          {{- if .hostPort }}
          hostPort: {{ .hostPort }}
          {{- end }}
        {{- end }}
        {{- end }}
        {{- if .Values.livenessProbe }}
        livenessProbe:
{{ toYaml .Values.livenessProbe | indent 10 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
        readinessProbe:
{{ toYaml .Values.readinessProbe | indent 10 }}
        {{- end }}
        {{- if .Values.resources }}
        resources:
{{ toYaml .Values.resources | indent 10 }}
        {{- end }}
      {{- if .Values.extraContainers }}
      {{- range .Values.extraContainers }}
      - name: extra
{{ toYaml . | indent 8 }}
        {{- if or $persistence $volumeMounts $secretVolumeMount $configmapVolumeMount }}
        volumeMounts:
        {{- if $configmapVolumeMount -}}
{{ $configmapVolumeMount | indent 10 }}
        {{- end }}
        {{- if $secretVolumeMount -}}
{{ $secretVolumeMount | indent 10 }}
        {{- end }}
        {{- if $volumeMounts }}
{{ toYaml $volumeMounts | indent 10 }}
        {{- end }}
        {{- if $persistence }}
        {{- range $persistence }}
          - name: {{ .name }}
            mountPath: {{ .path }}
        {{- end }}
        {{- end }}
        {{- end }}
        {{- if or $env $secrets $configs }}
        env:
        {{- if $env }}
{{ toYaml $env | indent 10 }}
        {{- end }}
        {{- if $secrets }}
          {{- range $secrets }}
          {{- if ne (default "env" .kind) "volume" }}
          - name: {{ .name }}
            valueFrom:
              secretKeyRef:
                key: {{ .key }}
                name: {{ $fullname }}
          {{- else }}
          - name: {{ .name }}
            value: {{ .path }}/{{ .key }}
          {{- end }}
          {{- end }}
        {{- end }}
        {{- if $configs }}
          {{- range $configs }}
          - name: {{ .name }}
            value: {{ .path }}/{{ .key }}
          {{- end }}
        {{- end }}
        {{- end }}
      {{- end }}
      {{- end }}
      {{- if eq (include "app.kind" .) "Job" }}
      restartPolicy: Never
      {{- end }}
      {{- if .Values.nodeSelector }}
      nodeSelector:
{{  toYaml .Values.nodeSelector | indent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations:
{{ toYaml .Values.tolerations | indent 6 }}
      {{- end }}
      {{- if or $persistence .Values.volumes (include "app.secretVolumes" .) (include "app.configmapVolumes" .) }}
      volumes:
      {{- if .Values.volumes }}
{{  toYaml .Values.volumes | indent 8 }}
      {{- end }}
      {{- if (include "app.secretVolumes" .) -}}
{{ include "app.secretVolumes" . | indent 8 }}
      {{- end }}
      {{- if (include "app.configmapVolumes" .) -}}
{{ include "app.configmapVolumes" . | indent 8 }}
      {{- end }}
      {{- if $persistence }}
      {{- range $persistence }}
        - name: {{ .name }}
          persistentVolumeClaim:
            claimName: {{ printf "%s-%s" $fullname .name | quote }}
      {{- end }}
      {{- end }}
      {{- end }}
