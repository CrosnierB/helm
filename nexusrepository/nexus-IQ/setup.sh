#!/usr/bin/env bash

set -e

workdir=$(dirname $0)
namespace=devops-tools
context=dev00-central-cluster-kikotey-com-admin
chart=sonatype/nexus-iq-server
name=nexus-repository-kikopool-cluster

function dry-run() {
  helm diff upgrade --kube-context ${context}  --install --namespace ${namespace} -f values.yaml ${name} ${chart}
}

function deploy() {
  helm upgrade --kube-context ${context}  --install --create-namespace --namespace ${namespace} -f values.yaml ${name} ${chart}
}

function confirm() {
  read -r -p "Rollout deployment ? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

function main() {
  #dry-run
  if confirm ; then
    deploy
  else
    echo "rollout abort"
  fi
}

main

