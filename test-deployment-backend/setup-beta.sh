#!/usr/bin/env bash
set -e

workdir=$(dirname $0)
namespace=workspace-beta-1608
context=dev00-central-cluster-kikotey-com
chart=app-0.9.7/app
name=beta-1608-test-backend-api

function dry-run() {
  helm diff upgrade -f values.yaml --namespace ${namespace} ${name} ${chart} --allow-unreleased
}

function deploy() {
  helm upgrade --kube-context ${context}  --install --create-namespace --namespace ${namespace} --render-subchart-notes -f values.yaml ${name} ${chart}
}

function confirm() {
  read -r -p "Rollout deployment ? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

helm repo update
function main() {
  dry-run
  if confirm ; then
    deploy
  else
    echo "rollout abort"
  fi
}

main
