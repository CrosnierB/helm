{{- define "app.kind" -}}
{{- $dictionary := dict "ds" "DaemonSet" "daemonset" "DaemonSet" "sts" "StatefulSet" "statefulset" "StatefulSet" "deploy" "Deployment" "deployment" "Deployment" "job" "Job" -}}
{{- default "Deployment" (get $dictionary (lower .Values.kind)) -}}
{{- end -}}